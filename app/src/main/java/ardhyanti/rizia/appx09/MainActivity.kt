package ardhyanti.rizia.appx09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay ->{
                audioPlay(posLaguSkrg)
            }
            R.id.btnNext ->{
                audioNext()
            }
            R.id.btnPrev ->{
                audioPrev()
            }
            R.id.btnStop ->{
                audioStop()
            }
        }
    }

    val daftarLagu = intArrayOf(R.raw.music_1, R.raw.music_2, R.raw.music_3)
    val daftarCover = intArrayOf(R.drawable.cover_1, R.drawable.cover_2, R.drawable.cover_3)
    val daftarJudul = arrayOf("Music 1","Music 2","Music 3")

    var posLaguSkrg = 0
    var handler = Handler()
    lateinit var  mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController

    lateinit var  db: SQLiteDatabase
    lateinit var adaplist : SimpleCursorAdapter
    lateinit var adapter : ListAdapter
    lateinit var  v : View
    var id_musik  =0
    var id_cover = ""
    var judul = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaPlayer = MediaPlayer.create(this, daftarLagu[posLaguSkrg])
        mediaController = MediaController(this)
        imV.setImageResource(daftarCover[posLaguSkrg])
        seekSong.max=100
        seekSong.progress=0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        db = DBOpenHelper(this).writableDatabase
        lsMusik.setOnItemClickListener(itemClick)
        dataMusik()
        listMusik()

    }

    fun dataMusik(): SQLiteDatabase {
        db= DBOpenHelper(this).writableDatabase
        return db
    }

    fun listMusik(){
        var sql = "select id_musik as _id , id_cover , judul_musik from musik"
        val c : Cursor = db.rawQuery(sql,null)
        adaplist = SimpleCursorAdapter(this,
            R.layout.list_music,
            c,
            arrayOf("_id","id_cover","judul_musik"),
            intArrayOf(R.id.txIdMusik,R.id.txIdCover,R.id.txJudulMusik),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsMusik.adapter = adaplist
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        judul = c.getString(c.getColumnIndex("judul_musik"))

        audioPlay(c.position)
    }

    fun milliSecondToString(ms : Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(pos :Int) {
        mediaPlayer = MediaPlayer.create(this, daftarLagu[pos])
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(daftarCover[pos])
        txJudulLagu.setText(daftarJudul[pos])
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread,50)
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying)mediaPlayer.stop()
        if(posLaguSkrg<(daftarLagu.size-1)){
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        audioPlay(posLaguSkrg)
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
        if(posLaguSkrg>0){
            posLaguSkrg--
        }else{
            posLaguSkrg = daftarLagu.size-1
        }
        audioPlay(posLaguSkrg)
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    inner class  UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress=currTime
            if(currTime != mediaPlayer.duration) handler.postDelayed(this, 50)

        }

    }
}
