package ardhyanti.rizia.appx09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper (context: Context): SQLiteOpenHelper(context,Db_name,null,Db_ver) {

    companion object{
        val Db_name = "music"
        val Db_ver = 1
    }


    override fun onCreate(db: SQLiteDatabase?) {
        val tmusik = "create table musik(id_musik Int primary key , id_cover Int not null , judul_musik text not null)"
        val insmusik = "insert into musik values ('0x7f0c0000','0x7f06005f','Music 1'),('0x7f0c0001','0x7f060060','Music 2'),('0x7f0c0002','0x7f060061','Music 3')"
        db?.execSQL(tmusik)
        db?.execSQL(insmusik)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}